# demoPages

This is my first Google Cloud app-website hwæs base is written in Python.
Coded for my practical training with Spain Internship (https://www.spain-internship.com/en/).
This is a test area before content goes to live site.
I started to study jQuery, Bootstrap and other things useful especially for development with WordPress and Joomla!

* Pull latest and publish GCloud command (note to myself):

git pull https://gitlab.com/mikko.timofeev/demoPages master; gcloud app deploy app.yaml --project spain-internship-test
-pages


* Public page on the Web:

https://spain-internship-test-pages.appspot.com/index.html
