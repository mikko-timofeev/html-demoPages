window.onload = window.onhashchange = setFooter3;

function setFooter3(e) // version with jQuery
{
	$('.active').removeClass('active');
	$('.chosenLink').removeClass('chosenLink');

	var newChoiceId = window.location.hash.substr(1);

	if (!($('#link_' + newChoiceId).hasClass('chosableLink'))) {
		newChoiceId = 'news';
	}
	
	$('#link_' + newChoiceId).addClass('active');
	$('#tab_' + newChoiceId).addClass('chosenLink');

	if (newChoiceId === 'news') {
		history.pushState('', document.title, window.location.pathname);
	}
	else
	{
		window.location.hash = newChoiceId;
	}

}